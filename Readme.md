Exercises regarding image similarity for the WS Digital Humanities at the TU-Berlin

* Exercises and solutions are to be found in image_ similarity_exercises.ipynb
* The image dataset to be used is contained in sphaera\_illustrations\_dataset
